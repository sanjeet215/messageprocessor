package com.asiczen.messageprocessor.testutil;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import org.json.JSONException;
import org.skyscreamer.jsonassert.JSONAssert;
import org.skyscreamer.jsonassert.JSONCompareMode;
import org.springframework.core.io.ClassPathResource;

public class JsonUtil {

    private final static String resourcePath = "src/test/resources/";

    public static ObjectMapper objectMapper = new ObjectMapper().registerModule(new JavaTimeModule())
            .configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS,false);

    public static boolean fileResourceExists(String fileName) {
        return new ClassPathResource(fileName).exists() || new File(fileName).exists();
    }

    public static <T> T readJsonFile(String filepath, Class<T> eClass) throws IOException {
        String content = Files.readString(Path.of(resourcePath + filepath), StandardCharsets.US_ASCII);
        return objectMapper.readValue(content, eClass);
    }

    public static void assetObject(Object actual, Object expected) throws JsonProcessingException, JSONException {
        String actualString = objectMapper.writeValueAsString(actual);
        String expectedString = objectMapper.writeValueAsString(expected);
        JSONAssert.assertEquals(expectedString, actualString, JSONCompareMode.LENIENT);
    }
}