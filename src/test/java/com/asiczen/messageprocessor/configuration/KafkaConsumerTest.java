package com.asiczen.messageprocessor.configuration;

import com.asiczen.messageprocessor.entity.BillingTracker;
import com.asiczen.messageprocessor.entity.MessageTracker;
import com.asiczen.messageprocessor.entity.WmMessage;
import com.asiczen.messageprocessor.mapservice.BillingTrackerMapService;
import com.asiczen.messageprocessor.mapservice.MessageTackerMapService;
import com.asiczen.messageprocessor.testutil.JsonUtil;
import java.io.IOException;
import org.json.JSONException;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class KafkaConsumerTest {

    @Autowired
    KafkaConsumer kafkaConsumer;

    @Autowired
    BillingTrackerMapService billingTrackerMapService;

    @Autowired
    MessageTackerMapService messageTackerMapService;

    @Test
    void wmMessageConsumer_1() throws IOException, JSONException {
        WmMessage wmMessage = JsonUtil.readJsonFile("wm/standardProcessing1/waterMeter.json", WmMessage.class);
        WmMessage wmMessage2 = JsonUtil.readJsonFile("wm/standardProcessing1/waterMeter_2.json", WmMessage.class);
        BillingTracker billingTracker = JsonUtil.readJsonFile("wm/standardProcessing1/billingTrackerOutput.json", BillingTracker.class);
        kafkaConsumer.wmMessageConsumer(wmMessage);
        kafkaConsumer.wmMessageConsumer(wmMessage2);
        BillingTracker billingTrackerActual = billingTrackerMapService.findBillRecordByDeviceUniqueId(wmMessage.getDeviceUniqueIdentifier());
        JsonUtil.assetObject(billingTrackerActual, billingTracker);
        MessageTracker messageTrackerActual = messageTackerMapService.findByDeviceId(wmMessage.getDeviceUniqueIdentifier());
        System.out.println(messageTrackerActual);
        MessageTracker messageTrackerExpected = JsonUtil.readJsonFile("wm/standardProcessing1/messageTracker.json",MessageTracker.class);
        JsonUtil.assetObject(messageTrackerActual,messageTrackerExpected);
    }

    @Test
    void wmMessageConsumer_2() throws IOException, JSONException {
        WmMessage wmMessage = JsonUtil.readJsonFile("wm/standardProcessing2/waterMeter.json", WmMessage.class);
        WmMessage wmMessage2 = JsonUtil.readJsonFile("wm/standardProcessing2/waterMeter_2.json", WmMessage.class);
        WmMessage wmMessage3 = JsonUtil.readJsonFile("wm/standardProcessing2/waterMeter_3.json", WmMessage.class);
        BillingTracker billingTracker = JsonUtil.readJsonFile("wm/standardProcessing2/billingTrackerOutput.json", BillingTracker.class);
        kafkaConsumer.wmMessageConsumer(wmMessage);
        kafkaConsumer.wmMessageConsumer(wmMessage2);
        kafkaConsumer.wmMessageConsumer(wmMessage3);
        BillingTracker billingTrackerActual = billingTrackerMapService.findBillRecordByDeviceUniqueId(wmMessage.getDeviceUniqueIdentifier());
        JsonUtil.assetObject(billingTrackerActual, billingTracker);
        MessageTracker messageTrackerActual = messageTackerMapService.findByDeviceId(wmMessage.getDeviceUniqueIdentifier());
        System.out.println(messageTrackerActual);
        MessageTracker messageTrackerExpected = JsonUtil.readJsonFile("wm/standardProcessing2/messageTracker.json",MessageTracker.class);
        JsonUtil.assetObject(messageTrackerActual,messageTrackerExpected);
    }
}
