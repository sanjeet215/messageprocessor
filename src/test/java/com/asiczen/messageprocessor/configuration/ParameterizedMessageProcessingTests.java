package com.asiczen.messageprocessor.configuration;

import com.asiczen.messageprocessor.entity.BillingTracker;
import com.asiczen.messageprocessor.entity.MessageTracker;
import com.asiczen.messageprocessor.entity.WmMessage;
import com.asiczen.messageprocessor.mapservice.BillingTrackerMapService;
import com.asiczen.messageprocessor.mapservice.MessageTackerMapService;
import com.asiczen.messageprocessor.testutil.JsonUtil;
import java.io.IOException;
import org.json.JSONException;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class ParameterizedMessageProcessingTests {

    private static final String filePath = "wmMessageProcessing/case";

    @Autowired
    KafkaConsumer kafkaConsumer;

    @Autowired
    BillingTrackerMapService billingTrackerMapService;

    @Autowired
    MessageTackerMapService messageTackerMapService;

    @ParameterizedTest
    @ValueSource(ints = {1})
    void testWmMessageProcessing(int testSequence) throws IOException, JSONException {
        WmMessage wmMessage = JsonUtil.readJsonFile(filePath+testSequence+"/waterMeter.json", WmMessage.class);
        BillingTracker billingTracker = JsonUtil.readJsonFile(filePath+testSequence+"/billingTrackerOutput.json", BillingTracker.class);
        kafkaConsumer.wmMessageConsumer(wmMessage);
        BillingTracker billingTrackerActual = billingTrackerMapService.findBillRecordByDeviceUniqueId(wmMessage.getDeviceUniqueIdentifier());
        JsonUtil.assetObject(billingTrackerActual, billingTracker);
        MessageTracker messageTrackerActual = messageTackerMapService.findByDeviceId(wmMessage.getDeviceUniqueIdentifier());
        System.out.println(messageTrackerActual);
        MessageTracker messageTrackerExpected = JsonUtil.readJsonFile(filePath+testSequence+"/messageTracker.json",MessageTracker.class);
        JsonUtil.assetObject(messageTrackerActual,messageTrackerExpected);
    }
}
