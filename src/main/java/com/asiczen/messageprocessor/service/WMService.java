package com.asiczen.messageprocessor.service;

import com.asiczen.messageprocessor.dto.Response;
import com.asiczen.messageprocessor.dto.WMMessageDTO;
import org.springframework.stereotype.Service;

@Service
public interface WMService {

    Response decodeProcessAndPublishToMessageQueue(WMMessageDTO wmMessageDTO);
}
