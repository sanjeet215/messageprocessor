package com.asiczen.messageprocessor.service;

import com.asiczen.messageprocessor.entity.WmMessage;
import org.springframework.stereotype.Service;

@Service
public interface ProcessWmMessageService {

    void processMessages(WmMessage wmMessage);
}
