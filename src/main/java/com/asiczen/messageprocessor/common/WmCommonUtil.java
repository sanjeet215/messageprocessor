package com.asiczen.messageprocessor.common;

import com.asiczen.messageprocessor.configuration.ApplicationConstant;
import com.asiczen.messageprocessor.dto.DecodedMessageDTO;
import java.util.Base64;
import java.util.Random;
import java.util.UUID;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public final class WmCommonUtil {

    private static final int MIN = Integer.parseInt(ApplicationConstant.RANDOM_NUMBER_MIN);
    private static final int MAX = Integer.parseInt(ApplicationConstant.RANDOM_NUMBER_MAX);

    private static final Random random = new Random();

    public static String RandomPartitionGenerator() {
        return String.valueOf(random.nextInt(MAX - MIN + 1) + MIN);
    }

    public static String idGenerator() {
        return UUID.randomUUID().toString();
    }

    public static DecodedMessageDTO decodeData(String inputData) {
        byte[] decodedBytes = Base64.getDecoder().decode(inputData);
        String decodedString = bytesToHex(decodedBytes);
        String meterReading = decodedString.substring(0, Math.min(decodedString.length(), 8));
        String cableStatus = decodedString.substring(8, Math.min(decodedString.length(), 10));
        String batteryStatus = decodedString.substring(10, Math.min(decodedString.length(), 12));

        Integer meterReadingValue = Integer.valueOf(meterReading, 16);
        Integer cableStatusValue = Integer.valueOf(cableStatus, 16);
        Integer batteryStatusValue = Integer.valueOf(batteryStatus, 16);

        DecodedMessageDTO waterMeterResponse = new DecodedMessageDTO();

        waterMeterResponse.setMeterReading(meterReadingValue);
        waterMeterResponse.setCableStatusValue(cableStatusValue);
        waterMeterResponse.setBatteryPercentage(calculateBatteryPercentage(batteryStatusValue));


        return waterMeterResponse;
    }

    private static int calculateBatteryPercentage(int batteryStatusValue) {
        return (int) ((batteryStatusValue / 255f) * 100);
    }


    private static final char[] HEX_ARRAY = "0123456789ABCDEF".toCharArray();

    public static String bytesToHex(byte[] bytes) {
        char[] hexChars = new char[bytes.length * 2];
        for (int j = 0; j < bytes.length; j++) {
            int v = bytes[j] & 0xFF;
            hexChars[j * 2] = HEX_ARRAY[v >>> 4];
            hexChars[j * 2 + 1] = HEX_ARRAY[v & 0x0F];
        }
        return new String(hexChars);
    }
}
