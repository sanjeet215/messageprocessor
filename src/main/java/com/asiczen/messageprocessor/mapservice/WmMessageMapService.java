package com.asiczen.messageprocessor.mapservice;

import com.asiczen.messageprocessor.entity.WmMessage;
import org.springframework.stereotype.Service;

@Service
public interface WmMessageMapService {

    WmMessage saveWmMessageInDatabase(WmMessage wmMessage);

    WmMessage getWmMessageByDeviceIdentifier(String deviceIdentifier);
}
