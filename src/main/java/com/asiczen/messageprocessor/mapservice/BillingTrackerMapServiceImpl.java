package com.asiczen.messageprocessor.mapservice;

import com.asiczen.messageprocessor.entity.BillingTracker;
import com.asiczen.messageprocessor.repository.BillingTrackerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class BillingTrackerMapServiceImpl implements BillingTrackerMapService {

    @Autowired
    BillingTrackerRepository billingTrackerRepository;

    @Override
    public BillingTracker saveOrUpdateBillingTracker(BillingTracker billingTracker) {
        return billingTrackerRepository.save(billingTracker);
    }

    @Override
    public BillingTracker findBillRecordByDeviceUniqueId(String deviceIdentifier) {
        return billingTrackerRepository.findByDeviceUniqueIdentifier(deviceIdentifier);
    }
}
