package com.asiczen.messageprocessor.mapservice;

import com.asiczen.messageprocessor.entity.WmMessage;
import com.asiczen.messageprocessor.repository.WmMessageRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class WmMessageMapServiceImpl implements WmMessageMapService{

    @Autowired
    WmMessageRepository wmMessageRepository;

    @Override
    public WmMessage saveWmMessageInDatabase(WmMessage wmMessage) {
        return  wmMessageRepository.save(wmMessage);
    }

    @Override
    public WmMessage getWmMessageByDeviceIdentifier(String deviceIdentifier) {
        return null;
    }
}
