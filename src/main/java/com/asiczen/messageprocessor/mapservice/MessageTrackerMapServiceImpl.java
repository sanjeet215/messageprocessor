package com.asiczen.messageprocessor.mapservice;

import com.asiczen.messageprocessor.entity.MessageTracker;
import com.asiczen.messageprocessor.repository.MessageTrackerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class MessageTrackerMapServiceImpl implements MessageTackerMapService {

    @Autowired
    MessageTrackerRepository messageTrackerRepository;

    @Override
    public MessageTracker saveOrUpdateMessageTracker(MessageTracker messageTracker) {
        return messageTrackerRepository.save(messageTracker);
    }

    @Override
    public MessageTracker findByDeviceId(String deviceId) {
        return messageTrackerRepository.findByDeviceID(deviceId);
    }
}
