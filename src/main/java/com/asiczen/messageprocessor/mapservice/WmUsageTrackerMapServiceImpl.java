package com.asiczen.messageprocessor.mapservice;

import com.asiczen.messageprocessor.entity.BillingTracker;
import com.asiczen.messageprocessor.repository.WmUsageTrackerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class WmUsageTrackerMapServiceImpl implements WmUsageTrackerMapService {

    @Autowired
    WmUsageTrackerRepository wmUsageTrackerRepository;

    @Override
    public BillingTracker saveWmMessageInDatabase(BillingTracker billingTracker) {
        return wmUsageTrackerRepository.save(billingTracker);
    }

    @Override
    public BillingTracker getWmMessageByDeviceIdentifier(String deviceIdentifier) {
        return wmUsageTrackerRepository.findByDeviceUniqueIdentifier(deviceIdentifier);
    }
}
