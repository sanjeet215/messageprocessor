package com.asiczen.messageprocessor.mapservice;

import com.asiczen.messageprocessor.entity.BillingTracker;

public interface BillingTrackerMapService {

    BillingTracker saveOrUpdateBillingTracker(BillingTracker billingTracker);

    BillingTracker  findBillRecordByDeviceUniqueId(String deviceIdentifier);
}
