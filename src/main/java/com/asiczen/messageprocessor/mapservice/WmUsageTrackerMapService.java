package com.asiczen.messageprocessor.mapservice;

import com.asiczen.messageprocessor.entity.BillingTracker;
import org.springframework.stereotype.Service;

@Service
public interface WmUsageTrackerMapService {

    BillingTracker saveWmMessageInDatabase(BillingTracker billingTracker);

    BillingTracker getWmMessageByDeviceIdentifier(String deviceIdentifier);
}
