package com.asiczen.messageprocessor.mapservice;

import com.asiczen.messageprocessor.entity.MessageTracker;
import org.springframework.stereotype.Service;

@Service
public interface MessageTackerMapService {

    MessageTracker saveOrUpdateMessageTracker(MessageTracker messageTracker);

    MessageTracker findByDeviceId(String deviceId);
}
