package com.asiczen.messageprocessor.repository;

import com.asiczen.messageprocessor.entity.BillingTracker;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BillingHistoryTrackerRepository extends JpaRepository<BillingTracker, String> {
}
