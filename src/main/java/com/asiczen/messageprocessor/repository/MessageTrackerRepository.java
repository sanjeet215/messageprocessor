package com.asiczen.messageprocessor.repository;

import com.asiczen.messageprocessor.entity.MessageTracker;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MessageTrackerRepository extends JpaRepository<MessageTracker, String> {
    MessageTracker findByDeviceID(String deviceId);
}
