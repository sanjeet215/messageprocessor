package com.asiczen.messageprocessor.repository;

import com.asiczen.messageprocessor.entity.BillingTracker;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BillingTrackerRepository extends JpaRepository<BillingTracker, String> {

    BillingTracker findByDeviceUniqueIdentifier(String deviceIdentifier);
}
