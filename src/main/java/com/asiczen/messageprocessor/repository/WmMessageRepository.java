package com.asiczen.messageprocessor.repository;

import com.asiczen.messageprocessor.entity.WmMessage;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface WmMessageRepository extends JpaRepository<WmMessage, String> {

    WmMessage findByDeviceUniqueIdentifier(String deviceUniqueIdentifier);
}
