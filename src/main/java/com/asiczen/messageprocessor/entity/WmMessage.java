package com.asiczen.messageprocessor.entity;

import com.fasterxml.jackson.annotation.JsonInclude;
import java.time.ZonedDateTime;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.Hibernate;


@Entity
@Table(name = "wmmessage")
@Getter
@Setter
@RequiredArgsConstructor
@ToString
@JsonInclude(JsonInclude.Include.NON_NULL)
public class WmMessage {
    @Id
    @Column(name = "message_id")
    private String wmMessageId;
    private String deviceName;
    private String deviceUniqueIdentifier;
    private long messageCounter;
    private String deviceAddress;
    private int meterReading;
    private boolean cableStatus;
    private double batteryPercentage;
    private ZonedDateTime timeStamp;
    private String tenantId;
    private String facilityId;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        WmMessage wmMessage = (WmMessage) o;
        return Objects.equals(wmMessageId, wmMessage.wmMessageId);
    }

    @Override
    public int hashCode() {
        return 0;
    }
}
