package com.asiczen.messageprocessor.entity;

import com.fasterxml.jackson.annotation.JsonInclude;
import java.time.LocalDateTime;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@Table(name = "messageTracker")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@JsonInclude(JsonInclude.Include.NON_NULL)
public class MessageTracker {

    @Id
    String messageId;
    int totalCountOfDay;
    int totalCountTillDate;
    String deviceID;
    LocalDateTime lastMessageReceivedAt;
    private String tenantId;
    private String facilityId;
}
