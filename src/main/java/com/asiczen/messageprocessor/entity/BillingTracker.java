package com.asiczen.messageprocessor.entity;

import com.fasterxml.jackson.annotation.JsonInclude;
import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@Table(name = "billingDetails")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@JsonInclude(JsonInclude.Include.NON_NULL)
public class BillingTracker implements Serializable {

    @Id
    @Column(name = "billingId")
    private String billingId;
    private String deviceUniqueIdentifier;
    private int currentReading;
    private LocalDate billingPeriodStartDate;
    private LocalDate billingPeriodEndDate;
    private int noOfUnitsConsumedInCurrentCycle;
    private String tenantId;
    private String facilityId;
    private LocalDateTime processedAt;
}
