package com.asiczen.messageprocessor.configuration;

import com.asiczen.messageprocessor.entity.WmMessage;
import com.asiczen.messageprocessor.service.ProcessWmMessageService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Slf4j
@Component
public class KafkaConsumer {

    @Autowired
    ProcessWmMessageService processWmMessageService;

    @KafkaListener(//groupId = ApplicationConstant.GROUP_ID_JSON,
            topics = ApplicationConstant.TOPIC_NAME,
            containerFactory = ApplicationConstant.KAFKA_LISTENER_CONTAINER_FACTORY)
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    //@Retryable(value = CannotAcquireLockException.class, backoff = @Backoff(delay = 100, maxDelay = 300), maxAttempts = 5)
    public void wmMessageConsumer(@Payload WmMessage wmMessage) {
        try {
            processWmMessageService.processMessages(wmMessage);
            log.trace("Json message received using Kafka listener {}", wmMessage);
        } catch (Exception exception) {
            log.error("Error while processing the message.... {} , error: {} ", wmMessage, exception.getMessage());
            exception.printStackTrace();
        }
    }
}
