package com.asiczen.messageprocessor.configuration;

public class ApplicationConstant {
    //public static final String TOPIC_NAME = "wm-message";
    public static final String TOPIC_NAME = "tc6zre3d-wmmessages";
    public static final String KAFKA_LISTENER_CONTAINER_FACTORY = "kafkaListenerContainerFactory";
    //public static final String GROUP_ID_JSON = "wm-message";

    public static final String RANDOM_NUMBER_MIN = "0";
    public static final String RANDOM_NUMBER_MAX = "5";

    public static final String SUCCESS_MESSAGE = "SUCCESSFULLY PROCESSED";
    public static final String ERROR_WHILE_DECODING_PUBLISHING = "Error while decoding or publishing to queue";
}
