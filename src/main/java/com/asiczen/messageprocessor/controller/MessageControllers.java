package com.asiczen.messageprocessor.controller;

import com.asiczen.messageprocessor.dto.Response;
import com.asiczen.messageprocessor.dto.WMMessageDTO;
import com.asiczen.messageprocessor.service.WMService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/service")
@Slf4j
public class MessageControllers {

    @Autowired
    private WMService wmService;

    @PostMapping("/wmmessage")
    public Response postMessageToQueue(@Validated @RequestBody WMMessageDTO wmMessageDTO) {
        return wmService.decodeProcessAndPublishToMessageQueue(wmMessageDTO);
    }
}
