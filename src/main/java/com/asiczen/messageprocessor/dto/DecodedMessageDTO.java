package com.asiczen.messageprocessor.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class DecodedMessageDTO {
    private int meterReading;
    private int cableStatusValue;
    private int batteryPercentage;
}
