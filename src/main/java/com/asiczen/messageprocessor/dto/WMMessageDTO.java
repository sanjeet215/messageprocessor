package com.asiczen.messageprocessor.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.time.ZonedDateTime;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class WMMessageDTO {

    private String deviceName;
    private String devEUI;
    private String data;
    @JsonProperty("fCnt")
    private long fCnt;
    private String devAddr;
    private ZonedDateTime time;
}
