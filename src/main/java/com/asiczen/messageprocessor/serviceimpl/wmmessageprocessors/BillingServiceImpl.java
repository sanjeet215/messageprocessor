package com.asiczen.messageprocessor.serviceimpl.wmmessageprocessors;

import com.asiczen.messageprocessor.common.WmCommonUtil;
import com.asiczen.messageprocessor.entity.BillingTracker;
import com.asiczen.messageprocessor.entity.WmMessage;
import com.asiczen.messageprocessor.mapservice.BillingTrackerMapService;
import com.asiczen.messageprocessor.service.WmUsageTrackerService;
import java.time.LocalDateTime;
import java.util.Objects;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Component
@Order(2)
@Slf4j
public class BillingServiceImpl implements WmUsageTrackerService {

    @Autowired
    BillingTrackerMapService billingTrackerMapService;

    @Override
    public void processMessages(WmMessage wmMessage) {

        log.trace(">> Billing calculation starts from here");
        if (wmMessage.getDeviceUniqueIdentifier() == null) {
            log.error("Error while processing the message device Identifier is null. {} ", wmMessage);
            return;
        }

        BillingTracker billingTracker = billingTrackerMapService.findBillRecordByDeviceUniqueId(wmMessage.getDeviceUniqueIdentifier());
        int unitsConsumed = 0;
        int noOfUnitsConsumedInCurrentCycle = 0;

        if (Objects.nonNull(billingTracker)) {
            unitsConsumed = wmMessage.getMeterReading() - billingTracker.getCurrentReading();
            noOfUnitsConsumedInCurrentCycle = billingTracker.getNoOfUnitsConsumedInCurrentCycle();

            if (unitsConsumed > 0) {
                billingTracker.setNoOfUnitsConsumedInCurrentCycle(noOfUnitsConsumedInCurrentCycle + unitsConsumed);
                billingTracker.setCurrentReading(wmMessage.getMeterReading());
            }
        } else {
            billingTracker = new BillingTracker();

            billingTracker.setBillingId(WmCommonUtil.idGenerator());
            billingTracker.setNoOfUnitsConsumedInCurrentCycle(0);
            billingTracker.setCurrentReading(wmMessage.getMeterReading());
            billingTracker.setDeviceUniqueIdentifier(wmMessage.getDeviceUniqueIdentifier());
        }

        billingTracker.setProcessedAt(LocalDateTime.now());

        billingTrackerMapService.saveOrUpdateBillingTracker(billingTracker);

        log.trace(">> Billing calculation ends here");

    }
}
