package com.asiczen.messageprocessor.serviceimpl.wmmessageprocessors;

import com.asiczen.messageprocessor.common.WmCommonUtil;
import com.asiczen.messageprocessor.entity.MessageTracker;
import com.asiczen.messageprocessor.entity.WmMessage;
import com.asiczen.messageprocessor.mapservice.MessageTackerMapService;
import com.asiczen.messageprocessor.service.WmUsageTrackerService;
import java.time.LocalDateTime;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Component
@Slf4j
@Order(1)
public class MessageTrackerProcessorServiceImpl implements WmUsageTrackerService {

    private final MessageTackerMapService messageTrackerMapService;

    @Autowired
    public MessageTrackerProcessorServiceImpl(MessageTackerMapService messageTrackerMapService) {
        this.messageTrackerMapService = messageTrackerMapService;
    }

    @Override
    public void processMessages(WmMessage wmMessage) {

        log.trace(">> Message tracker processing started....");

        if (!StringUtils.isNotBlank(wmMessage.getDeviceUniqueIdentifier())) {
            log.error("Received an invalid message to process so rejecting it. Message: {}", wmMessage);
            return;
        }

        if (wmMessage.getDeviceUniqueIdentifier() != null) {
            MessageTracker messageTracker = messageTrackerMapService.findByDeviceId(wmMessage.getDeviceUniqueIdentifier());
            if (messageTracker != null) {
                int dayCount = messageTracker.getTotalCountOfDay();
                int tillDateCount = messageTracker.getTotalCountTillDate();
                messageTracker.setTotalCountOfDay(dayCount + 1);
                messageTracker.setTotalCountTillDate(tillDateCount + 1);
            } else {
                messageTracker = new MessageTracker();
                messageTracker.setMessageId(WmCommonUtil.idGenerator());
                messageTracker.setTotalCountOfDay(1);
                messageTracker.setTotalCountTillDate(1);
                messageTracker.setDeviceID(wmMessage.getDeviceUniqueIdentifier());
            }

            messageTracker.setLastMessageReceivedAt(LocalDateTime.now());
            messageTrackerMapService.saveOrUpdateMessageTracker(messageTracker);
        }

        log.trace("<< Message tracker processing ends here ....");
    }
}
