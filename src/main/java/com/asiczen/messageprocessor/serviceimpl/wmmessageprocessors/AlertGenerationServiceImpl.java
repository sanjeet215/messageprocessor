package com.asiczen.messageprocessor.serviceimpl.wmmessageprocessors;

import com.asiczen.messageprocessor.entity.WmMessage;
import com.asiczen.messageprocessor.service.WmUsageTrackerService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Component
@Order(3)
@Slf4j
public class AlertGenerationServiceImpl implements WmUsageTrackerService {

    @Override
    public void processMessages(WmMessage wmMessage) {

    }
}
