package com.asiczen.messageprocessor.serviceimpl;

import com.asiczen.messageprocessor.entity.WmMessage;
import com.asiczen.messageprocessor.mapservice.WmUsageTrackerMapService;
import com.asiczen.messageprocessor.service.ProcessWmMessageService;
import com.asiczen.messageprocessor.service.WmUsageTrackerService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ProcessWmMessageServiceImpl implements ProcessWmMessageService {

    @Autowired
    List<WmUsageTrackerService> wmUsageTrackerServices;

    private final WmUsageTrackerMapService wmUsageTrackerMapService;

    @Autowired
    public ProcessWmMessageServiceImpl(WmUsageTrackerMapService wmUsageTrackerMapService) {
        this.wmUsageTrackerMapService = wmUsageTrackerMapService;
    }

    @Override
    public void processMessages(WmMessage wmMessage) {
        for (WmUsageTrackerService wmUsageTrackerService : wmUsageTrackerServices) {
            wmUsageTrackerService.processMessages(wmMessage);
        }
    }
}
