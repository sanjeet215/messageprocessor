package com.asiczen.messageprocessor.serviceimpl;

import com.asiczen.messageprocessor.common.WmCommonUtil;
import com.asiczen.messageprocessor.configuration.ApplicationConstant;
import com.asiczen.messageprocessor.configuration.KafkaProducer;
import com.asiczen.messageprocessor.dto.DecodedMessageDTO;
import com.asiczen.messageprocessor.dto.Response;
import com.asiczen.messageprocessor.dto.WMMessageDTO;
import com.asiczen.messageprocessor.entity.WmMessage;
import com.asiczen.messageprocessor.mapservice.WmMessageMapService;
import com.asiczen.messageprocessor.service.WMService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Component
@Slf4j
public class WMServiceImpl implements WMService {

    private final KafkaProducer kafkaProducer;

    private final WmMessageMapService wmMessageMapService;

    @Autowired
    public WMServiceImpl(KafkaProducer kafkaProducer, WmMessageMapService wmMessageMapService) {
        this.kafkaProducer = kafkaProducer;
        this.wmMessageMapService = wmMessageMapService;
    }


    @Override
    @Transactional(propagation = Propagation.REQUIRES_NEW, readOnly = false)
    public Response decodeProcessAndPublishToMessageQueue(WMMessageDTO wmMessageDTO) {

        try {
            WmMessage wmMessage = extractWMMessage(wmMessageDTO);
            wmMessageMapService.saveWmMessageInDatabase(wmMessage);
            kafkaProducer.sendMessageToTopic(ApplicationConstant.TOPIC_NAME, WmCommonUtil.RandomPartitionGenerator(), wmMessage);
            return new Response(ApplicationConstant.SUCCESS_MESSAGE, HttpStatus.CREATED);
        } catch (Exception exception) {
            log.error("Message Processing failed with following error {} ", exception.getMessage());
            exception.printStackTrace();
            return new Response(ApplicationConstant.ERROR_WHILE_DECODING_PUBLISHING, HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }

    private WmMessage extractWMMessage(WMMessageDTO wmMessageDTO) {
        WmMessage wmMessage = new WmMessage();
        wmMessage.setWmMessageId(WmCommonUtil.idGenerator());
        wmMessage.setDeviceName(wmMessageDTO.getDeviceName());
        wmMessage.setMessageCounter(wmMessageDTO.getFCnt());
        wmMessage.setDeviceUniqueIdentifier(wmMessageDTO.getDevEUI());
        wmMessage.setDeviceAddress(wmMessageDTO.getDevAddr());
        DecodedMessageDTO response = WmCommonUtil.decodeData(wmMessageDTO.getData());
        wmMessage.setBatteryPercentage(response.getBatteryPercentage());
        wmMessage.setMeterReading(response.getMeterReading());
        wmMessage.setCableStatus(response.getCableStatusValue() == 0 ? Boolean.TRUE : Boolean.FALSE);
        wmMessage.setTimeStamp(wmMessageDTO.getTime());
        return wmMessage;
    }
}
